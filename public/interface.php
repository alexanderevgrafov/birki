<?php
function getDir() {
    return __DIR__ . "/db";
}

function getParam($name) {
    return @$_GET[$name];
}

function Lst() {
    $dir = getDir();
    $list = [];

    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            $f = explode(".", $file);
            if ($f[1] == "json") {
                $data = json_decode(file_get_contents($dir . "/" . $file), true);

                if ($data && $data["id"] == $f[0]) {
                    $list[] = $data;
                }
            }
        }
        closedir($dh);
    }

    return json_encode($list);
}

function Add($data) {
    $id = $data["id"];
    $filename = getDir() . "/" . $id . ".json";
    if ($f = fopen($filename, "w")) {
        fwrite($f, json_encode($data));
        fclose($f);
    } else {
        throw new Exception("Error opening file " . $id);
    }
}

function Del($id) {
    $filename = getDir() . "/" . $id . ".json";
    unlink($filename);
}

try {
    $body = file_get_contents('php://input');
    $post = json_decode($body, true);

    $postOp = @$post['op'];


    if ($postOp == "add" || $postOp == "update") {
        $data = $post['data'];
        Add($data);
        echo '{ "ok": true, "data":' . json_encode($data) . '}';
    }

    if ($postOp == "order") {
        $dt = $post['data'];

        $createdAt = date("Y-m-d H:i");
        $order = $dt['orderData'];
        $maket = $dt['maketData'];
        $subject = 'Maket print order';
        $to_email = "alexanderevgrafov@yandex.ru, alexander.evgrafov@gmail.com, birki@signogroup.ru";
        $from_name = "SignoGroup Sticker Composer";
        $from_email = "zakaz@birki.signogroup.ru";
        $boundaryStr = 'BND09utl23';
        $headers = <<<EOD1
From: $from_name <$from_email>
Reply-to: $from_name <$from_email>
Return-path: $from_email
Precedence: bulk
MIME-Version: 1.0
Content-Type: multipart/mixed; charset=UTF-8; boundary=$boundaryStr
Content-Transfer-Encoding: 8bit
EOD1;

        $usedImagesHtml = '';
        $usedImagesBase64 = '';
        $usedTexts = '';

        foreach ($maket['texts'] as $index => $txt) {
            $usedTexts .= "<li><strong>{$txt['text']}</strong>, Шрифт: {$txt['fontFamily']}, {$txt['fontSize']}мм</li>";
        }

        foreach ($maket['images'] as $index => $image) {
            $usedImagesBase64 .= <<<EODimg
--$boundaryStr
Content-Type: image/png
Content-Disposition: attachment; filename="image$index.png"
Content-ID: <image$index>
Content-Transfer-Encoding: base64
X-Attachment-Id: image$index

{$image}

EODimg;
            $usedImagesHtml .= "<p><img src='cid:image$index' alt='Used image #$index'/></p>\n";
        }

        $clientCardPart = '';

        if ($order['clientCard']) {
            $clientCardPart .= <<<EODimg
--$boundaryStr
Content-Type: {$order['clientCardMime']}
Content-Disposition: attachment; filename="{$order['clientCardName']}"
Content-Transfer-Encoding: base64

{$order['clientCard']}

EODimg;
        }

        $shipping = $order['shippingRequired'] ? $order['shippingAddress'] : 'не заказана';
        $clientCard = $order['clientCard'] ? 'прикреплена' : 'не предоставлена';

        $msg = <<<EOD2

 This is MIME letter

--$boundaryStr
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: 8bit

<h4>Информация от заказе</h4>
<p><i>Материал:</i> {$order['materialDescription']}</p>
<p><i>Количество:</i> {$order['quantity']}</p>
<p><i>Карточка клиента:</i> {$clientCard}</p>
<p><i>Имя:</i> {$order['name']}</p>
<p><i>Телефон:</i> {$order['phone']}</p>
<p><i>Email:</i> {$order['email']}</p>
<p><i>Доставка:</i> {$shipping}</p>
<hr>
<h4>Макет</h4>
<p><i>Создан $createdAt</i></p>
<p><img src='cid:preview1' alt='Maket preview image'/></p>
$usedTexts
$usedImagesHtml
  
--$boundaryStr
Content-Type: image/png
Content-Disposition: attachment; filename="testimage.png"
Content-ID: <preview1>
Content-Transfer-Encoding: base64
X-Attachment-Id: preview1

{$maket['preview']}

$usedImagesBase64
$clientCardPart
--$boundaryStr--
EOD2;

//           echo "[$headers]\n";
//            echo "[$msg]\n";

        $mailResult = mail($to_email, $subject, $msg, $headers);

        echo '{ "ok": true, "data":' . $mailResult . '}';
    }

    $getOp = getParam("op");

    if ($getOp == "list") {
        $list = Lst();
        echo '{ "ok": true, "data": ' . $list . '}';
    } else if ($getOp == "del") {
        $id = getParam("id");
        Del($id);
        $list = Lst();
        echo '{ "ok": true, "data": ' . $list . '}';
    }
} catch (Exception $err) {
    echo "<h3>" . $err . "</h3>";

    echo '{ "ok": false, "err": ' . $err . '}';
}
