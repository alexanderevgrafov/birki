const ADMIN_PATH = '/interface.php';

const _validationMessageCore = ( _error, asString ) => {
  if( !_error ) {
    return asString ? '' : [];
  }

  let ret = [], error = _error.nested;

  _error.error && ret.push( _error.error );

  for( let key in error ) {
    if( error.hasOwnProperty( key ) ) {
      const val = error[ key ];
      if( val instanceof Object && val.length && val.nested ) {
        ret = _.union( ret, validationMessage( val, false ) )
      } else {
        val !== 'Required' && ret.push( val );
      }
    }
  }

  return asString ? ret.join( '; ' ) : ret;
};

export const validationMessage = ( _error, asString = true ) => {
  if( Array.isArray( _error ) ) {
    const ret = _.flatten( _error.map( x => _validationMessageCore( x, false ) ) );

    return asString ? ret.join( '; ' ) : ret;
  } else {
    return _validationMessageCore( _error, asString );
  }
};

export  {
  ADMIN_PATH,
  validationMessage,
}
