import {define, Record, shared, type} from 'type-r';
import {fitDimensions} from './Images';
import {obj2style} from './htmlUtils';
import {ADMIN_PATH} from './Utils';

@define
export class BirkaProps extends Record {
  static attributes = {
    price: 1000,
    description: '',
  }
}

@define
export class BirkaModel extends Record {
  static attributes = {
    label: '',
    svg: '',
    variants: BirkaProps.Collection,
    _sx: 0,
    _sy: 0,
  }

  get sx() {
    return this._sx / 10;
  }

  get sy() {
    return this._sy / 10;
  }

  set sx(x) {
    this._sx = Math.round(parseFloat(x) * 10);
  }

  set sy(y) {
    this._sy = Math.round(parseFloat(y) * 10);
  }

  get src() {
//    return '../data/svg/' + this.fileName;
    return this.svg;
  }

  get variantsText(){
    return this.variants.map(v=>v.price+'='+v.description).join("\n");
  }

  set variantsText(x){
    const match=(x + "\n").matchAll(/(\d+)=(.*?)\n/g);
    const data = _.map([...match], x=>({price:x[1],description: x[2]}));

    this.variants.reset(data);
  }

  save(options) {
    const json = this.toJSON();
    const body = JSON.stringify({op: 'add', data: json});

    return fetch(ADMIN_PATH, {method: 'POST', body});
  }

  del() {
    if (confirm('Вы уверены что хотите удалить этот макет?')) {
      return fetch(ADMIN_PATH + '?op=del&id=' + this.id,).then(
        () => {
          this.collection.remove(this);
        }
      );
    }
  }

  static collection = {
    load() {
      return fetch(ADMIN_PATH + '?op=list')
        .then(res => res.json())
        .then(json => {
          this.reset(json.data, {parse: true});
        })
        .catch(err => alert(err.message || err))

      //     const json = JSON.parse(birki_load);

      //      const json = birki_load.birki;
      //      this.reset(json, {parse:true});
    }
  }
}

@define
export class MaketModel extends Record {
  static attributes = {
//    maxW: type(Number).value(400).watcher('recalculateDimensions'),
//    maxH: type(Number).value(400).watcher('recalculateDimensions'),
    pixelsFor100: type(Number).value(400).watcher('recalculateDimensions'),
    birka: shared(BirkaModel).watcher('onBirkaChange'),
    turn: type(Number).value(0).watcher('recalculateDimensions'),
    flip: type(Boolean).value(false).watcher('recalculateDimensions'),
    containerStyle: type(Object).toJSON(false),
    birkaBgStyle: type(Object).toJSON(false),
    zoom: type(Number).value(100).watcher('recalculateDimensions'),
  }

  get sx() {
    return this.birka ? (this.turn % 180) ? this.birka.sy : this.birka.sx : 0;
  }

  get sy() {
    return this.birka ? (this.turn % 180) ? this.birka.sx : this.birka.sy : 0;
  }

  get maketStyle() {
    return {
      transform: 'rotate(' + this.turn + 'deg) scale(' + (this.flip ? -1 : 1) + ',1)',
    }
  }

  onBirkaChange(){
    const {birka} = this;

    if (!birka) {
      return;
    }

    const max = Math.max(birka.sx, birka.sy);

    this.pixelsFor100 = (max / 25.4)*72;  //  Assuming 72dpi as logical(css) screen;
  }

  recalculateDimensions(){
    const {birka, pixelsFor100} = this;

    if (!birka) {
      return;
    }

    const [bw, bh] = fitDimensions(birka.sx, birka.sy, pixelsFor100);

    this.containerStyle  = {width: pixelsFor100, height: pixelsFor100, transform: 'scale('+(this.zoom/100)+')'};
    this.birkaBgStyle = Object.assign({
      width: bw,
      height: bh,
      marginTop: (pixelsFor100 - bh) / 2,
      marginLeft: (pixelsFor100 - bw) / 2,
    }, this.maketStyle);
  }

  getBirkaHtml(){
    return  `<div style="${obj2style(this.birkaBgStyle)}">
       <img src="${this.birka.src}" width="100%" height="100%"/>
     </div>`;
  }

  toJSON(options) {
    const json = super.toJSON(options);
    json.bid = this.birka && this.birka.id;
    return json;
  }
}
