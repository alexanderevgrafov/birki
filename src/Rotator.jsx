import React from 'react-mvx';
import {define, shared} from 'type-r';
import { useHistory } from "react-router-dom";
import {Button} from './ui/Bootstrap';
import {AppState, BirkaModel} from './ApplicationState';
import {fitDimensions} from './Images';

export const Rotator = ({appState}) => {
  const  history = useHistory();
  const {maket} = appState;
  const {birka, maketStyle} = maket;
  const [w, h] = birka && fitDimensions(birka.sx, birka.sy, 400);

  function doTurn(deg) {
    maket.turn = (maket.turn + deg)%360;
  }

  function doFlip() {
    maket.flip = !maket.flip;
  }

  function goEdit() {
    history.push('/edit');
//    appState.mode = 'edit'
  }

  function goBack() {
    history.goBack();
  }

  return <div id="page-rotator">
    { birka ?
      <div id="rotator-box" style={maketStyle}>
        <img src={birka.src} width={w} height={h}/>
      </div> : <h3>Необходимо выбрать вариант макета</h3>
    }
    { birka ? <>
      <Button label="Turn left" onClick={() => doTurn(-90)}/>
      <Button label="Turn right" onClick={() => doTurn(90)}/>
      <Button label="Flip" onClick={() => doFlip()}/>
      <Button label="Edit areas" onClick={() => goEdit()}/>
    </> : null
    }
    <Button label="Выбор макетов" onClick={() => goBack()}/>
  </div>
}


export {Rotator}
