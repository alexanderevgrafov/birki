import React from 'react-mvx';
import {define, Record, type} from 'type-r';
import {AreaModel} from './AreaModel';
import {fitDimensions} from './Images';
import {obj2style} from './htmlUtils';
import {BirkaModel, MaketModel} from './MaketModel';
import {ADMIN_PATH} from './Utils';


@define
export class AppState extends Record {
  static attributes = {
    areas: type(AreaModel.Collection).watcher('onStoreChanges'),
    birki: BirkaModel.Collection,
    maket: type(MaketModel).watcher('onStoreChanges'),
  }

  constructor() {
    super();

    this.birki.load().then(() => {
      this.lsLoad();
    });

    this.lsSave = _.throttle(this.lsSaveOriginal, 2000);
  }

  onStoreChanges() {
    this.lsSave();
  }

  lsSaveOriginal() {
    try {
      const maket = this.maket.toJSON();
      const areas = this.areas.toJSON();

      localStorage.setItem('lastMaket', JSON.stringify(maket));
      localStorage.setItem('lastAreas', JSON.stringify(areas));

    } catch (e) {
      alert('Smth wrong with LS save');

      console.log('Smth wrong with LS save', e);
    }
  }

  lsLoad() {
    try {
      const maket = localStorage.getItem('lastMaket');
      const areas = localStorage.getItem('lastAreas');

      if (maket) {
        const {bid, ...json} = JSON.parse(maket);
        this.maket.set(json, {parse: true});
        this.maket.birka = this.birki.get(bid);
        this.maket.recalculateDimensions();
      }
      if (areas) {
        this.areas.reset(JSON.parse(areas), {parse: true});
      }
    } catch (e) {
      alert('Smth wrong with LS load');

      console.log('Smth wrong with LS load', e);
    }
  }

  generateHtml() {
    const {maket, areas} = this;
    const [width, height] = fitDimensions(maket.sx, maket.sy, maket.maxW, maket.maxH);
    const bgStyle = maket.birkaBgStyle;
    const {marginTop, marginLeft} = bgStyle;
    const areasHtml = areas.map(area => area.getHtml(marginLeft, marginTop)).join('\n');
    const birkaHtml = maket.getBirkaHtml();
    const containerStyle = {
      width, height,
      position: 'relative',
    }
    return `
    <div style="${obj2style(containerStyle)}">
    ${birkaHtml}
    ${areasHtml}
    </div>
    `;
  }

  doSendOrder(orderData, maketData) {
    const json = {orderData, maketData};
    const body = JSON.stringify({op: 'order', data: json});

//    console.log(body);
 //   return Promise.resolve();
    return fetch(ADMIN_PATH, {method: 'POST', body});
  }
}

