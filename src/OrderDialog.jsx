import React                          from 'react-mvx';
import {define, Record, shared, type} from 'type-r';
import {Button, Form, Modal}          from './ui/Bootstrap';
import {AppState}                     from './ApplicationState';
import { FileUploader }               from "./Images";
import { validationMessage }           from "./Utils";

@define
export class OrderDialogState extends Record {
  static attributes = {
    materialDescription : "",
    offerta           : type( Boolean ).value( false ).check( x => x, "Пожалуйста примите политику конфиденциальности" ),
    quantity          : type( Number ).value( 1 ).check( x => x > 0, "Минимальный заказ 1000 шт." ),
    shippingRequired  : false,
    shippingAddress   : "",
    clientCard        : "",
    clientCardName    : "",
    clientCardMime    : "",
    email             : "",
    phone             : "",
    name              : "",
  }

  toJSON( options ) {
    const json = super.toJSON( options );

    json.quantity *= 1000;

    return json;
  }
}

const getPureBase64 = urlData => urlData.split(',')[1];

const getImages = areas => _.map(
  areas.filter(area =>
    area.type === 'image'), area => getPureBase64(area.image));

const getTexts = areas => _.map(
    areas.filter(area =>
    area.type === 'text'), area =>
    _.pick(area, ('text,fontFamily,fontSize').split(',')));

@define
class OrderDialog extends React.Component {
  static props = {
    appState: type(AppState),
    onClose: Function,
    imgData: String,
  }

  static state = OrderDialogState

  componentDidMount() {
    const variant = this.props.appState.maket.birka.variants.last();

    if (variant) {
      this.state.materialDescription = variant.description;
    }
  }

  doSendOrder = () => {
    const orderData = this.state.toJSON();
    const {areas} = this.props.appState;
    const maketData = {
      preview: getPureBase64(this.props.imgData),
      images: getImages(areas),
      texts: getTexts(areas)
    }

    this.props.appState.doSendOrder(orderData, maketData)
      .then(async res => res.json())
      .then(data => {
        if (data.ok) {
          alert('Заявка с макетом успешно отправлена. Мы свяжемся с вами в ближайшее время!');

          this.props.onClose();
        } else {
          throw new Error('Какая-то ошибка на сервере');
        }
      })
      .catch(err => alert('Ошибка: ' + (err.message || err)));
  }

  getPrice() {
    const { birka } = this.props.appState.maket;
    const { state } = this;

    const birkaProps = _.find(birka.variants.models, x=>x.description === state.materialDescription );

    return Math.max(0, birkaProps ? Math.round(state.quantity * birkaProps.price) : 0);
  }

  render() {
    const {onClose, imgData} = this.props;
    const {birka} = this.props.appState.maket;
    const {state} = this;
    const price = this.getPrice();
    const offerUrl = _.get( window, "constructorConfig.agreementUrl", "");
    const validationErrorsText = validationMessage( this.state.validationError );

    return <Modal show={true} onHide={onClose} backdrop="static" centered>
      <Modal.Header>
        <Modal.Title>Отправить заявку</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Row label="Варианты материала">
          <Form.ControlLinked valueLink={state.linkAt('materialDescription')} as="select">
            { birka.variants.map( x => <option key={ x } value={ x.description }>{ x.description } ({x.price} руб за тысячу)</option> ) }
          </Form.ControlLinked>
        </Form.Row>
        <Form.Row label="Количество (шт)" units="тыс. штук">
          <Form.ControlLinked valueLink={state.linkAt('quantity')} type="number"/>
        </Form.Row>
        {price ? <Form.Row label="Предварительная цена">{ price } руб.</Form.Row> : null }
        <Form.Row label="Доставка">
          <Form.CheckLinked type="checkbox" valueLink={state.linkAt('shippingRequired')}/>
        </Form.Row> {
        state.shippingRequired ?
          <Form.Row label="Адрес доставки">
            <Form.ControlLinked valueLink={state.linkAt('shippingAddress')} as="textarea"/>
          </Form.Row> :
          void 0
      }
        <Form.Row label="Ваше имя">
          <Form.ControlLinked valueLink={state.linkAt('name')}/>
        </Form.Row>
        <Form.Row label="Контактный email">
          <Form.ControlLinked valueLink={state.linkAt('email')}/>
        </Form.Row>
        <Form.Row label="Контактный телефон">
          <Form.ControlLinked valueLink={state.linkAt('phone')}/>
        </Form.Row>
        <Form.Row label="Карточка клиента">
          {state.clientCard ? 'Прикреплена' : ''}
        <FileUploader
            onUpload={(data, name, type) => {
              state.clientCard = data;
              state.clientCardName = name;
              state.clientCardMime = type;
            }}
            maxSize={2*1024*1024}
            resize={false}
            label={state.clientCard ? 'Заменить' : 'Прикрепить' }
        />
        </Form.Row>
        <Form.Row>
          <Form.CheckLinked
            valueLink={ state.linkAt( "offerta" ) } type='checkbox'
            label={
              <>Согласен с <a href={ offerUrl } target='_blank'>Политикой конфиденциальности персональных данных</a>
              </>
            }/>
        </Form.Row>
        <img src={ imgData } alt='Preview image' style={{maxWidth:'100%'}}/>
      </Modal.Body>
      <Modal.Footer>
        {
          validationErrorsText ? <div className="form-error-text">{validationErrorsText}</div> : void 0
        }
        <Button variant="secondary"
                onClick={onClose}>
          Отмена
        </Button>
        <Button variant="primary"
                onClick={this.doSendOrder}
                disabled={ !!validationErrorsText }>
          Отправить
        </Button>
      </Modal.Footer>
    </Modal>
  }
}


export {OrderDialog}
