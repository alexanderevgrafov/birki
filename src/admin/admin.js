import React from 'react-mvx'
import * as ReactDOM from 'react-dom';
import {AdminApp} from './Admin';


ReactDOM.render(<AdminApp />,  document.getElementById('root'));
