import React from 'react-mvx';
import {define} from 'type-r';
import {BirkaModel} from '../ApplicationState';
import {BirkaList} from '../Selector';
import {Button, Form, Col} from '../ui/Bootstrap';
import {TextUploader} from '../Images';

import '../styles/app.scss'

@define
export class AdminState extends Record {
  static attributes = {
    birki: BirkaModel.Collection,
    selected: BirkaModel,
  }

  constructor() {
    super();

    this.birki.load();
  }
}


@define
export class BirkaForm extends React.Component {
  static props = {
    model: BirkaModel,
  }

  onSvgUpload = (data, fileName) => {
    const {model} = this.props;

    model.svgTextData = data;
    model.fileName = fileName;
  }

  render() {
    const {model} = this.props;

    return <div id="birka-form">
      <Col>
        <Form.Row label="Название">
          <Form.Control valueLink={model.linkAt('label')}/>
        </Form.Row>
        <Form.Row label="Ширина (мм)">
          <Form.Control valueLink={model.linkAt('sx')}/>
        </Form.Row>
        <Form.Row label="Высота (мм)">
          <Form.Control valueLink={model.linkAt('sy')}/>
        </Form.Row>
        <Form.Row label="Файл SVG">
          <TextUploader onUpload={this.onSvgUpload}/>
        </Form.Row>
        <Form.Row>

        </Form.Row>
      </Col>
    </div>
  }
}

@define
export class AdminApp extends React.Component {
  static state = AdminState;

  onSelect = birka => this.state.selected = birka;

  addNew = () => this.onSelect(new BirkaModel());

  onDelete = e => {
    alert('Implement delete!');
  }

  onUpdate = e => {
    const {selected} = this.state;

    selected.save();
  }

  render() {
    const {birki, selected} = this.state;
    const isNew = !(selected && birki.find(selected));

    return <div id="admin-page">
      {birki.length ? <BirkaList birki={birki} selected={selected} onSelect={this.onSelect}/> : 'Список пуст'}
      {selected && <>
        <BirkaForm model={selected}/>
        <Button label={isNew ? 'Create' : 'Update'} onClick={this.onUpdate}/>
        <Button label="Delete" onClick={this.onDelete}/>
      </>}
      <Button label="Add new" onClick={() => this.addNew()}/>
    </div>;
  }
}
