import React from 'react-mvx';
import {define, type, shared} from 'type-r';
import {Birka} from './Selector';
import {Button, Form} from './ui/Bootstrap';
import {BirkaModel} from './MaketModel';
import {uuid} from 'uuidv4'
import './styles/app.scss'
import {FileUploader} from './Images';

@define
class EditForm extends React.Component {
  static props = {
    model: BirkaModel,
  }

  static state = {
    variants: ''
  }

  componentDidMount() {
    this.state.variants = this.props.model.variantsText;
    this.state.on('change:variants', () => this.props.model.variantsText = this.state.variants);
  }

  render() {
    const {model, onSave, onCancel} = this.props;

    return <div id="admin-edit-form">
      <Form.Row label="Метка для списка">
        <Form.ControlLinked valueLink={model.linkAt('label')}/>
      </Form.Row>
      <Form.Row label="Ширина (мм)">
        <Form.ControlLinked valueLink={model.linkAt('sx')}/>
      </Form.Row>
      <Form.Row label="Высота (мм)">
        <Form.ControlLinked valueLink={model.linkAt('sy')}/>
      </Form.Row>
      <Form.Row label="Файл">
        {model.svg && <img src={model.svg} width={100} height={100} alt='preview' style={{margin:10}}/>}
        <FileUploader
          onUpload={text => model.svg = text}
          allowedFormats={['image/svg+xml']}
          label={model.svg ? 'Заменить SVG файл' : 'Загрузить SVG файл'}
          resize={false}
        />
      </Form.Row>
      <Form.Row label="Толщина и цена">
        <Form.ControlLinked valueLink={this.state.linkAt('variants')} as="textarea"/>
        <div className="hint">
        <br/>В каждой строке в формате
        <br/>&lt;цена_за_тысячу&gt;=&lt;описание&gt;
        </div>
      </Form.Row>
      <Form.Row>
        <Button label="Сохранить" onClick={() => onSave()}/>
        <Button label="Отмена" onClick={() => onCancel()}/>
      </Form.Row>
    </div>;
  }
}

@define
class AdminApp extends React.Component {
  static state = {
    birki: BirkaModel.Collection,
    selected: type(BirkaModel).value(null),
  }

  componentDidMount() {
    this.state.birki.load();
  }

  createNew = () => {
    this.state.selected = new BirkaModel({id: uuid()});
  }

  onSelect = model => this.state.selected = model.clone();

  onDel = model => model.del();

  doCancel = () => this.state.selected = null;

  doSave = () => this.state.selected.save()
    .then(res => res.json())
    .then(res => {
      this.state.birki.load();
      this.doCancel()
    });

  render() {
    const {birki, selected} = this.state;

    return <>
      <Button label="Создать новый" onClick={() => this.createNew()}/>
      <div id="page-selector">{
        !birki.length ? 'Нет ни одного макета' :
        <div id="maket-select-list" className="admin-list">{
          birki.map( item =>
            <div key={ item.id }>
              <Birka item={ item } isSelected={ selected === item } onSelect={ _.noop }/>
              <div className='hint'>
                { item.variants.map( x => <li key={ x }>{ x.description} *1000 = { x.price }руб</li> ) }
              </div>
              <div>
                <Button label='Редактировать' onClick={ () => this.onSelect( item ) }/>
                <Button label='Удалить' onClick={ () => this.onDel( item ) }/>
              </div>
            </div>
          )
        }</div>
      }
      </div>
      {selected ?
        <EditForm model={selected} onSave={() => this.doSave()} onCancel={() => this.doCancel()}/> :
        void 0
      }
    </>;
  }
}

export {AdminApp};
