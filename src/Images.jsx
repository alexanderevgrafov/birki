import React from 'react-mvx';
import {Button} from './ui/Bootstrap';

export const fitDimensions = (w1,h1,_maxW,_maxH=0, noEnlarge = false) =>{
  const maxW = _maxW;
  const maxH = _maxH || maxW;
  const r1   = h1 && w1 ? w1 / h1 : 1;
  const rm   = maxH ? maxW / maxH : 1;
  if( rm > r1 ) {
    return noEnlarge && h1 < maxH ? [ w1, h1 ] : [ Math.ceil( maxH * r1 ), maxH ];
  } else {
    return noEnlarge && w1 < maxW ? [ w1, h1 ] : [ maxW, Math.ceil( maxW / r1 ) ];
  }
}

export const FileUploader = ({onUpload, label = 'Выбрать файл', allowedFormats = [], maxSize = 0, resize = true, maxDimension = 900}) => {
  const doLoad = e => {
    const file = e.target.files[0];

    if (maxSize && file.size > maxSize) {
      const sizes = ['b', 'Kb', 'Mb', 'Gb'];
      let index = 0;
      let test = maxSize;

      while (test>1024 && index < sizes.length) {
        index++;
        test/=1024;
      }
      alert('Максимально допустимый размер файла ' + (Math.round(test*10)/10) + sizes[index]);
    } else
    if (!allowedFormats.length || _.indexOf( allowedFormats, file.type)>=0) {
      const reader = new FileReader();

      if (resize) {
        const image = document.createElement('img');
        const canvas = document.createElement('canvas');

        canvas.width = 300;
        canvas.height = 300;

        image.onload = () => {
          const context = canvas.getContext('2d');
          const w1 = image.width;
          const h1 = image.height;

          [canvas.width, canvas.height] = fitDimensions(w1,h1,Math.max(maxDimension, 900), 0, true);

          context.drawImage(image, 0, 0, w1, h1, 0, 0,canvas.width, canvas.height);

          const resized = canvas.toDataURL('image/png');

          onUpload(resized, file.name, 'image/png');
        }
        reader.onload = event => image.src = event.target.result;
      } else {
        reader.onload = event => onUpload(event.target.result, file.name, file.type);
      }

      reader.readAsDataURL(file);
    } else {
      alert('Похоже, неправильный формат файла');
    }
  }

  return <div className="upload-input">
    <Button label={label} className="over-button"/>
    <input type="file" onChange={doLoad} className="real-button"/>
    </div>
}

export const ImagePreview = ({area}) => <div className="propbox-image-preview">{
  area.type === 'image' && area.image ? <img alt="preview" src={area.image} style={{maxWidth:150, maxHeight:150}}/> : void 0}
</div>


