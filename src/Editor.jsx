import React from 'react-mvx';
import {define, type, shared} from 'type-r';
import {Area, SelectedArea, PropBox, AreaSelectContext} from './Area';
import {Button, Form, Row, Col} from './ui/Bootstrap';
import {AppState} from './ApplicationState';
import {AreaModel} from './AreaModel';
//import {Slider} from './ui/Slider';
import {OrderDialog} from './OrderDialog';
import {fitDimensions} from './Images';

@define
class Editor extends React.Component {
  static props = {
    appState: type(AppState),
  }

  static state = {
    zoomMode: 100, //type(Number).value(0).watcher(function(){}),
    hoveredArea: shared(AreaModel),
    selectedArea: shared(AreaModel),
    testHtml: {__html: ''},
    orderPreviewData: '',
    screenShoting: false,
  }

  componentDidMount() {
    this.state.on('change:zoomMode', () => this.onPageResize());
    $(window).on('resize', this.onPageResize);
    this.onPageResize();
  }

  componentWillUnmount() {
    $(window).off('resize', this.onPageResize);
  }

  onPageResize = () => {
    const {zoomMode} = this.state;
    const {maket} = this.props.appState;

    if (!zoomMode) {
      const $layEditor = $('#layout-editor')
      const available = Math.min($layEditor.width(), $layEditor.height());

      maket.zoom = 100 * available / maket.pixelsFor100;
    } else {
      maket.zoom = zoomMode;
    }
  }

  addNewArea(type) {
    const area = new AreaModel({type, text: ''})

    this.props.appState.areas.add(area);
    this.onAreaSelect(area);
  }

  onAreaSelect = (area, e) => {
    if (!area && this.state.selectedArea) {
      this.state.selectedArea.deselect();
    }
    this.state.selectedArea = area;
    e && e.stopPropagation();
  }

  onAreaHover = (area) => {
    this.state.hoveredArea = area;
  }

  onAreaRemove = area => {
    if (confirm('Удалить ' + (area.type === 'text' ? 'этот текст' : 'это изображение') + '?')) {
      const {areas} = this.props.appState;
      const {selectedArea} = this.state;

      if (selectedArea === area) {
        this.state.selectedArea = null;
      }

      areas.remove(area);
    }
  }

  doTurn = (deg) => {
    const {maket} = this.props.appState;

    maket.turn = (maket.turn + deg);
  }

  doFlip = () => {
    const {maket} = this.props.appState;

    maket.flip = !maket.flip;
  }

  goSelector() {
    this.props.history.push('/');
  }

  doSendOrder = () => {
    this.sendDialogClose();
  }

  makeOrder = () => {
    this.onAreaSelect(null);
    this.makeScreenshot().then(imgData => this.state.orderPreviewData = imgData)
  }

  makeScreenshot = () => {
    const canvasPad = 45;
    this.state.screenShoting = true;

    return new Promise(resolve => {
      setTimeout(() => {
        const elem = document.querySelector('#screenshot-content');
        const svg = document.getElementById('areas-bg-birka');
        const canvas = document.createElement('canvas');
        const {turn, flip, birka, pixelsFor100: boxDim} = this.props.appState.maket;
        const [bw, bh] = fitDimensions(birka.sx, birka.sy, boxDim);
        const [cw, ch] = turn % 180 ? [bh, bw] : [bw, bh];

        canvas.width = cw + canvasPad * 2;
        canvas.height = ch + canvasPad * 2;

        const ctx = canvas.getContext('2d');

        ctx.translate(cw / 2 + canvasPad, ch / 2 + canvasPad);
        ctx.rotate(turn * Math.PI / 180);
        ctx.transform(flip ? -1 : 1, 0, 0, 1, 0, 0);
        ctx.drawImage(svg, -bw / 2 /*+ canvasPad*/, -bh / 2 /*+ canvasPad*/, bw, bh);//, turn % 180 ? cw : ch, turn % 180 ? ch : cw);
        ctx.rotate(0);
        ctx.resetTransform();
        ctx.translate(-(boxDim - cw) / 2 + canvasPad, -(boxDim - ch) / 2 + canvasPad);

        html2canvas(elem, {canvas, backgroundColor: null, scale:1 })
          .then(canvas => {
           this.state.screenShoting = false;
            resolve(canvas.toDataURL());
          });
      }, 50);
    })
  }

//  Test = () => this.makeScreenshot().then(data => $('#testCanvas').attr('src', data));

  sendDialogClose = () => {
    this.state.orderPreviewData = '';
  }

  render() {
    const {appState} = this.props;
    const {selectedArea, hoveredArea, orderPreviewData, screenShoting} = this.state;
    const {maket, areas} = appState;
    const {birka, containerStyle, birkaBgStyle} = maket;
    const imageAreasCount = areas.filter(area=>area.type==="image").length;

    return <div id="page-editor">
      <div id="layout-toolbar">
        <Row>
          <Col>
            <Button label="Выбрать другой макет" onClick={() => this.goSelector()}/>
          </Col>
          {birka ? <>
            <Col>
              <Button label="Повернуть" onClick={() => this.doTurn(90)}/>
              <Button label="Отразить" onClick={() => this.doFlip()}/>
            </Col>
            <Col>Масштаб:
              <Form.ControlLinked valueLink={this.state.linkAt('zoomMode')} as="select">
                <option value={0}>Максимальное увеличение</option>
                <option value={100}>100%</option>
                <option value={150}>150%</option>
                <option value={200}>200%</option>
              </Form.ControlLinked>
            </Col>
            <Col md={6}>

            </Col>
          </> : null
          }
        </Row>
        {/*        <Button label="Проверить" onClick={() => this.Test()}/>*/}
      </div>
      <div id="layout-row">
        <div id="layout-editor">
          {
            birka ? <div id="editor-container" style={containerStyle} onDoubleClick={() => this.onAreaSelect(null)}>
              <img id="areas-bg-birka" style={birkaBgStyle} src={birka.src} alt=''/>
              <div id="areas-group">
                <AreaSelectContext.Provider value={[this.onAreaSelect, this.onAreaHover]}>
                  {
                    areas.map((area, indx) => selectedArea === area || hoveredArea === area ?
                      <SelectedArea
                        area={area} key={indx}
                        isClicked={selectedArea === area}
                        isHovered={hoveredArea === area}
                        zoom={maket.zoom}
                      /> : <Area area={area} key={indx}/>
                    )
                  }
                </AreaSelectContext.Provider>
              </div>
              {/*         {'Zoom ' + maket.zoom + '%'}*/}
            </div> : null
          }
        </div>
        {screenShoting ?
          <div id="screenshot-base">
            <div id="screenshot-content">
              <AreaSelectContext.Provider value={[this.onAreaSelect, this.onAreaHover]}>
                {
                  areas.map((area, indx) => selectedArea === area || hoveredArea === area ?
                    <SelectedArea
                      area={area} key={indx}
                      isClicked={selectedArea === area}
                      isHovered={hoveredArea === area}
                      zoom={100}
                    /> : <Area area={area} key={indx}/>
                  )
                }
              </AreaSelectContext.Provider>
            </div>
          </div> : null
        }
        <div id="layout-props">
          <Row>
            <Button onClick={() => this.addNewArea('text')} label="Добавить текст"/>
          </Row>
          <Row>
            <Button onClick={() => this.addNewArea('image')} label="Добавить изображение" disabled={imageAreasCount >= 10}/>
          </Row>
          <Row>{
            selectedArea ? <PropBox area={selectedArea} onRemove={() => this.onAreaRemove(selectedArea)}/> : void 0
          }</Row>
        </div>
      </div>
      <div id="layout-bottom">
        <Button label="Заказать" onClick={() => this.makeOrder()}/>
      </div>
{/*      <canvas id="test-canva"/>*/}

      {orderPreviewData && <OrderDialog appState={appState} imgData={orderPreviewData} onClose={this.sendDialogClose}/>}
    </div>
  }
}

export {Editor};
