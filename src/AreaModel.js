import React from 'react-mvx';
import {define, Record, type} from 'type-r';
import {obj2style} from './htmlUtils';

@define
export class AreaModel extends Record {
  static attributes = {
    x: 0,
    y: 0,
    width: 200,
    height: 100,
    type: 'text',
    image: '',
    text: 'New text',
    fontFamily: 'Arial',
    fontSize: 8.0,
  }

  getImageDataUrl() {
    return 'data:image/png;base64,' + this.image;
  }

  get isText(){
    return this.type === "text";
  }

  get isImage(){
    return this.type === "image";
  }

  getHtml(dx, dy) {
    const {type, text, x, y, width, height} = this;
    const style = {width, height, top: y - dy, left: x - dx, position: 'absolute', overflow: 'hidden'};

//    style.border = '1px dashed blue';

    if (type === 'text') {
      style.fontFamily = this.fontFamily;
      style.fontSize = this.fontSize + 'mm';

      return '<div style=\'' + obj2style(style) + '\'>' + text + '</div>';
    } else {
      style.backgroundImage = 'url(' + this.image + ')';
      style.backgroundRepeat = 'no-repeat';
      style.backgroundPosition = 'center center';
      style.backgroundSize = 'contain';
      return '<div style=\'' + obj2style(style) + '\'></div>';
    }
  }

  isEmpty(){
    if (this.isText && this.text.trim() === "") {
      return true;
    }
    if (this.isImage && !this.image) {
      return true;
    }

    return false;
  }

  deselect() {
    if (this.isEmpty()) {
      const collection = this.collection;

      collection.remove(this);
    }
  }

  static collection = {}
}
