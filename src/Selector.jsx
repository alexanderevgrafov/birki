import React from 'react-mvx';
import {define, shared} from 'type-r';
import {Button} from './ui/Bootstrap';
import {AppState} from './ApplicationState';
import {BirkaModel} from './MaketModel';
import {fitDimensions} from './Images';
import cx from 'classnames'

const Birka = ({item, isSelected, onSelect}) => {
  const {sx, sy, label, src} = item;
  const [x, y] = fitDimensions(sx, sy, 200);

  return <div className={cx('birka', {'selected': isSelected})} onClick={isSelected ? null : () => onSelect(item)}>
    <div className="label">{label} <span className="size">({sx}x{sy})</span></div>
    <img src={src} width={x} height={y} alt=''/>
  </div>
}

@define
class Selector extends React.Component {
  static props = {
    appState: AppState,
  }

  static state = {
    selected: shared(BirkaModel)
  }

  onBirkaSelect = item => {
    this.state.selected = item;
  }

  goEdit = () => {
    const {appState} = this.props;

    appState.maket.birka = this.state.selected;
    appState.lsSave();
    this.props.history.push('/edit');
//    appState.mode = 'rotate';
  }

  render() {
    const {birki} = this.props.appState;

    return <div id='page-selector'>{
      !birki.length ? "Нет макетов для отображения" :
      <>
        <h2>Доступные варианты бирок</h2>
        <div id='maket-select-list'>
          {
            birki.map( item => {
                const isSelected = this.state.selected === item;
                return <div className='item' key={ item.id }>
                  <Birka item={ item } isSelected={ isSelected } onSelect={ this.onBirkaSelect }/>
                  { isSelected ? <Button label='Выбрать макет' onClick={ () => this.goEdit() }/> : null }
                </div>
              }
            )
          }
        </div>
      </>
    }
    </div>
  }
}

export {
  Selector,
  Birka,
}
