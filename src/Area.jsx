import React from 'react-mvx';
import {Rnd} from 'react-rnd'
import {Form, Col, Row, Button} from './ui/Bootstrap';
import {Slider} from './ui/Slider'
import {ImagePreview, FileUploader} from './Images';
import {useContext} from 'react';
import cx from 'classnames';

const AreaSelectContext = React.createContext(null);

const AreaText = ({area}) => {
  const {text, fontFamily, fontSize} = area;
  const style = {fontFamily, fontSize: fontSize + 'mm'}

  return <div className="area text" style={style}>{text}</div>
}

const AreaImage = ({area}) => {
  const style = {
    backgroundImage: 'url(' + area.image + ')',
  }

  return <div className="area image" style={style}/>
}

const AreaCore = ({area}) => {
  const [onSelect, onHover] = useContext(AreaSelectContext);

  return <div className="area-mouse-ev"
              onMouseEnter={e => onHover(area, e)}
              onMouseLeave={e => onHover(null, e)}
              onClick={e => onSelect(area, e)}
  >{
    area.type === 'image' ? <AreaImage area={area}/> : <AreaText area={area}/>
  }
  </div>
}

const Area = ({area}) => {
  const {x, y, width, height} = area;
  const style = {width, height, top: y, left: x};

  return <div className="area-frame" style={style}>
    <AreaCore area={area}/>
  </div>
}

const SelectedArea = ({area, isClicked, isHovered, zoom}) => {
  const [onSelect] = useContext(AreaSelectContext);
  const {x, y, width, height} = area;

  return <Rnd size={{width, height}}
              position={{x, y}}
              scale={zoom / 100}
              onDragStart={() => onSelect(area)}
              onDragStop={(e, d) => {
                area.set({x: d.x, y: d.y})
              }}
              onResizeStart={() => onSelect(area)}
              onResizeStop={(e, direction, ref, delta, position) => {
                area.set({
                  width: parseInt(ref.style.width),
                  height: parseInt(ref.style.height),
                  ...position,
                });
              }}
    //  bounds="parent"
              className={cx('area-frame', {selected: isClicked, hovered: isHovered})}
              resizeHandleClasses={{
//        bottom?: string;
        bottomLeft: "resize-hdl bl",
        bottomRight: "resize-hdl br",
//        left?: string;
//        right?: string;
//        top?: string;
        topLeft: "resize-hdl tl",
        topRight: "resize-hdl tr",
    }}
  >
    <AreaCore area={area}/>
  </Rnd>
}

const PropBoxTyped = ({area}) => area.type === 'image' ? <PropBoxImage area={area}/> : <PropBoxText area={area}/>

const PropBox = ({area, onRemove}) => {
  return <div className="prop-box">
    <Row>
      <Col>
        <PropBoxTyped area={area}/>
        <Button label="Удалить" onClick={() => onRemove()}/>
      </Col>
    </Row>
  </div>
}

const PropBoxText = ({area}) =>
  <>
    <Form.Row label="Текст">
      <Form.ControlLinked valueLink={area.linkAt('text')}/>
    </Form.Row>
    <Form.Row label="Шрифт">
      <Form.ControlLinked valueLink={area.linkAt('fontFamily')} as="select">
        <option>Arial</option>
        <option>Arial Black</option>
        <option>Calibri</option>
        <option>Impact</option>
        <option>Times New Roman</option>
        <option>Georgia</option>
        <option>Comic Sans MS</option>
      </Form.ControlLinked>
    </Form.Row>
    <Form.Row label={'Размер ' + Math.round(area.fontSize*10)/10 + 'мм'}>
      <Slider valueLink={area.linkAt('fontSize')}
              step={.5}
              min={3}
              max={100}/>,
    </Form.Row>
  </>

const PropBoxImage = ({area}) =>
  <Form.Row label="Image">
    <ImagePreview area={area}/>
    <FileUploader
      onUpload={data => area.image = data}
      allowedFormats={['image/jpeg', 'image/png']}
      label={(area.image ? 'Заменить' : 'Загрузить') + ' изображение'}
    />
  </Form.Row>

export {Area, SelectedArea, PropBox, AreaSelectContext};
