const num2px = val => _.isNumber(val) ? (val + 'px') : val;
const camel2kebab = txt => txt.replace(/[A-Z]/g, x => '-' + x.toLowerCase());
const obj2style = obj => _.map(obj, (val, key) => camel2kebab(key) + ':' + num2px(val)).join('; ')

export {
  obj2style
}
