import React from 'react-mvx'
import * as ReactDOM from 'react-dom';
import {AdminApp} from './AdminApp';

ReactDOM.render(<AdminApp />,  document.getElementById('root')
);
