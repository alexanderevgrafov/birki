import React from 'react-mvx';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import {Editor} from './Editor';
import {Selector} from './Selector';
import {define} from 'type-r';
import {AppState} from './ApplicationState';
import "./styles/app.scss"

@define
class App extends React.Component {
  static state = AppState;
  render() {
    return <>
      <Router>
        <Switch>
          <Route exact path="/" render={routeProps=><Selector appState={this.state} {...routeProps} />}/>
          <Route path="/edit" render={routeProps=><Editor appState={this.state} {...routeProps} />}/>
        </Switch>
      </Router>
    </>;
  }
}

export default App;
